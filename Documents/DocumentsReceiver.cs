﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Documents
{
    public delegate void DocumentsReadyHandler(object obj, FileSystemEventArgs e);
    public delegate void TimeOutHandler();

    public class DocumentsReceiver
    {
        public event DocumentsReadyHandler DocumentsReady;
        public event TimeOutHandler TimedOut;

        private readonly Timer _timer;
        private readonly FileSystemWatcher _fileSystemWatcher;
        private Dictionary<string, bool> _files = new Dictionary<string, bool>();

        public DocumentsReceiver(IEnumerable<string> filesWatching)
        {
            _timer = new Timer();
            _fileSystemWatcher = new FileSystemWatcher();
            filesWatching.ToList().ForEach(f => _files.Add(f, false));
        }
        public void Start(string targetDirectory, int waitingInterval)
        {
            _fileSystemWatcher.Path = targetDirectory;
            _fileSystemWatcher.EnableRaisingEvents = true;
            _fileSystemWatcher.Created += CreateDelete;
            _fileSystemWatcher.Deleted += CreateDelete;

            _timer.Interval = waitingInterval;
            _timer.Elapsed += timerElapsed;
            _timer.Start();
        }

        private void CreateDelete(object sender, FileSystemEventArgs e)
        {
            if (_files.ContainsKey(e.Name))
            {
                _files[e.Name] = (e.ChangeType == WatcherChangeTypes.Created);
                DocumentsReady?.Invoke(this, e);
                if (FilesAllLoaded)
                    UnsetSubscribtion();

            }
        }

        private void UnsetSubscribtion()
        {
            _timer.Elapsed -= timerElapsed;
            _fileSystemWatcher.Created -= CreateDelete;
            _fileSystemWatcher.Deleted -= CreateDelete;
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            TimedOut?.Invoke();
            UnsetSubscribtion();
        }

        public bool FilesAllLoaded { get => _files.Values.All(x => x == true); }
    }
}